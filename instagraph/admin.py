from django.contrib import admin
from instagraph.models import *
# Register your models here.

@admin.register(InstagramUser)
class InstaUserAdmin(admin.ModelAdmin):
    fields = ['username', 'password', 'email', 'last_used', 'current']

@admin.register(Follower)
class FollowerAdin(admin.ModelAdmin):
    fields = ['username', 'insta_id', 'followers']
    list_display = ['username', '__str__']
