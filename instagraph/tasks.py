from celery import shared_task

from instagraph.models import InstagramUser, Follower

from bs4 import BeautifulSoup
import json, random, re, requests
from django.utils import timezone
from datetime import timedelta
import time
import traceback


def delay_time(m=5):
    return timezone.now() + timedelta(minutes=m)


@shared_task
def fetch_follower(username):
    return x + y

@shared_task
def process_username(username, level=2, followers=True):
    # Get the user longest not used
    # Maybe we are already processing this username
    user = InstagramUser.objects.filter(current__username=username)
    if not user.count() == 1:
        query = InstagramUser.objects.filter(current__isnull=True).order_by('last_used')
        # If we don't have a user at the moment, we wait
        if query.count() == 0:
            print("No user found, waiting.")
            process_username.apply_async((username, level, followers), eta=delay_time())
            return
        print("Starting with user " + query[0].username)
        user = query[0]
    else:
        print("Continuing with user " + user[0].username)
        user = user[0]
    # Select user and prepare
    user.last_used = timezone.now()
    user.save()
    user.process_followers = followers
    session = prepare_fetch(username, user)

    # Start the loop for the followers
    print("User: " + user.username)
    process_batch(user, level, session, followers)
    print("Finished processing! (" + user.username + ")")

    return
    

def process_batch(insta_user, level, session, followers):
    for u, created in fetch_followers(insta_user, session, level, followers):
        t = (insta_user.current, u) if insta_user.process_followers else (u, insta_user.current)
        add_edge(*t)
        print("Added edge: " + t[1].username + "-->" + t[0].username )
        if level > 1 and created:
            # I don't know how we should decide, whether to  fetch followers or following...
            print("Added to queue: " + u.username )
            process_username.apply_async((u.username, level - 1, False))

def fetch_followers(insta_user, session, level, followers):
    # Make request and return results
    has_next_page = True
    url = 'https://www.instagram.com/graphql/query/'
    while has_next_page:
        variables = json.dumps({
            'id': str(insta_user.current.insta_id),
            'after': insta_user.cursor or '',
            'first': "40",
            'include_reel': False
        })
        data = {
            'query_hash': insta_user.get_hash(),
            'variables': variables
        }

        # Here we need error-handling, if we get rate limited
        print("Request page.")
        r = session.get(url, params=data)
        if r.status_code != 200:
            print("Error code, abort and retry!")
            process_username.apply_async((insta_user.current.username, level, followers), eta=delay_time(20))
            return

        edge_name = 'edge_followed_by' if insta_user.process_followers else 'edge_follow'
        page_info = r.json()['data']['user'][edge_name]['page_info']
        has_next_page = page_info['has_next_page']
        users = r.json()['data']['user'][edge_name]['edges']

        for user in users:
            node = user['node']
            insta_id = node['id']
            username = node['username']
            u, c = Follower.objects.get_or_create(insta_id=insta_id, defaults={'username': username})
            yield u, c

        if has_next_page:
            insta_user.cursor = page_info['end_cursor']
            insta_user.save()
            time.sleep(5)
    print("Last page reached")
    insta_user.cursor = ''
    insta_user.current = None
    insta_user.last_used = timezone.now()
    insta_user.save()
    return


def add_edge(f1, f2):
    f1.followers.add(f2)
    print(f2.username + '-->' + f1.username)
    f1.save()
    f2.save()


HEADERS_LIST = [
    "Mozilla/5.0 (Windows NT 5.1; rv:41.0) Gecko/20100101"\
    " Firefox/41.0",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2)"\
    " AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2"\
    " Safari/601.3.9",
    "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0)"\
    " Gecko/20100101 Firefox/15.0.1",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36"\
    " (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36"\
    " Edge/12.246"
]

BASE_URL = 'https://www.instagram.com/accounts/login/'
LOGIN_URL = BASE_URL + 'ajax/'


def prepare_fetch(username, insta_user):
    session = login_user(insta_user)
    url = 'https://www.instagram.com/' + username

    request = session.get(url)
    soup = BeautifulSoup(request.content, 'html.parser')
    body = soup.find('body')
    pattern = re.compile('window._sharedData')

    script = body.find("script", text=pattern)

    script = script.get_text().replace('window._sharedData = ', '')[:-1]
    data = json.loads(script)

    links = soup.find_all('link')
    consumer_link = ""
    for link in links:
        if 'Consumer.js' in link['href']:
            consumer_link = 'https://www.instagram.com' + link['href']
            break

    script = session.get(consumer_link).text

    m = re.search(r"t=\"(?P<t>\w+)\",n=\"(?P<n>\w+)\"", script)

    # While we're at it
    insta_user.follower_hash = m.group('t')
    insta_user.following_hash = m.group('n')


    current_id = data['entry_data']['ProfilePage'][0]['graphql']['user']['id']
    f, c = Follower.objects.get_or_create(insta_id=current_id, defaults={'username': username})
    f.save()
    insta_user.current = f
    insta_user.save()

    return session

def login_user(insta_user):
    username = insta_user.username
    password = insta_user.password
    useragent = HEADERS_LIST[random.randint(0, len(HEADERS_LIST)-1)]

    session = requests.Session()
    session.headers = {'user-agent': useragent}
    session.headers.update({'Referer': BASE_URL})    
    req = session.get(BASE_URL)    
    soup = BeautifulSoup(req.content, 'html.parser')    
    body = soup.find('body')

    pattern = re.compile('window._sharedData')
    script = body.find("script", text=pattern)

    script = script.get_text().replace('window._sharedData = ', '')[:-1]
    data = json.loads(script)

    csrf = data['config'].get('csrf_token')
    login_data = {'username': username, 'password': password}
    session.headers.update({'X-CSRFToken': csrf})
    login = session.post(LOGIN_URL, data=login_data, allow_redirects=True)

    return session
