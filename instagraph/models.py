from django.db import models
from datetime import datetime

# Create your models here.

class InstagramUser(models.Model):
    username = models.CharField(max_length=140)
    password = models.CharField(max_length=140)

    email = models.EmailField()

    last_used = models.DateTimeField(default=datetime.now)

    follower_hash = models.CharField(max_length=50, blank=True)
    following_hash = models.CharField(max_length=50, blank=True)

    current = models.ForeignKey('Follower', models.CASCADE, blank=True, null=True)
    process_followers = models.BooleanField(default=False)

    cursor = models.CharField(max_length=200, blank=True)

    def get_hash(self):
        return self.follower_hash if self.process_followers else self.following_hash

    def __str__(self):
        return self.username


class Follower(models.Model):
    # Nodes
    username = models.CharField(max_length=140)
    insta_id = models.IntegerField(db_index=True)

    last_updated = models.DateTimeField(auto_now=True)

    followers = models.ManyToManyField("self", symmetrical=False, related_name="following")

    def __str__(self):
        return self.username + str(self.followers.count())

class IndexedFollower(models.Model):
    experiment = models.ForeignKey("Experiment", models.CASCADE)
    follower = models.ForeignKey(Follower, models.CASCADE)
    index = models.IntegerField()

class Experiment(models.Model):
    label = models.CharField(max_length=180)
    subset = models.ManyToManyField(Follower, through=IndexedFollower)


class Dimension(models.Model):
    label = models.CharField(max_length=180)
    index = models.IntegerField()
    experiment = models.ForeignKey(Experiment, models.CASCADE)

class DimensionValue(models.Model):
    dimension = models.ForeignKey(Dimension, models.CASCADE)
    value = models.SmallIntegerField()
    relation = models.ForeignKey(IndexedFollower, models.CASCADE)
