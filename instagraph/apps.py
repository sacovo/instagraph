from django.apps import AppConfig


class InstagraphConfig(AppConfig):
    name = 'instagraph'
